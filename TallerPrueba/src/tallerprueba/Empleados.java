/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerprueba;
import java.util.ArrayList;

/**
 *
 * @author CltControl
 */
public class Empleados {
      
    String nombre;
    int tiempoTrabajo;
    int horasExtra;
    ArrayList<Habitacion> habitaciones;
    
    public Empleados(String nombre, int tiempoTrabajo, int horasExtra,ArrayList<Habitacion> habitaciones){
        this.nombre=nombre;
        this.tiempoTrabajo=tiempoTrabajo;
        this.horasExtra=horasExtra;
        this.habitaciones=habitaciones;               
    }
    
    public double Sueldo(){
        int c_gold=0;
        int c_vip=0;
        int c_normal=0;
        for (Habitacion h: habitaciones){
            if (h.rango=="gold"){
                c_gold+=1;
            }
            if (h.rango=="vip"){
                c_vip+=1;
            }
            if (h.rango=="normal"){
                c_normal+=1;
            }                        
        }
        if (tiempoTrabajo<18){
            double sueldo=(c_gold*12)+(c_vip*10)+(c_normal*8)+(horasExtra*3)+200;
            return sueldo;
        }else{
            double sueldo=(c_gold*12)+(c_vip*10)+(c_normal*8)+(horasExtra*4)+200;
            return sueldo;
            
        }
    }
    
    
    
    
    
}
